import React, { Component } from 'react';

class NotFound extends Component {
    render() {
        return (
            <div className='p-5 m-5 d-flex justify-content-center' style={{height:'80vh', width:'80vw'}}>
                <img src='https://cdn.dribbble.com/users/1408464/screenshots/6377404/404_illustration_4x.png' alt='error-Img' />
            </div>
        );
    }
}

export default NotFound;