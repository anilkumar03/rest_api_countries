import { Link } from "react-router-dom"
import React, { Component } from 'react';

class CountryDetails extends Component {

    render() {
        return (
            <>
                <div>
                    <Link to='/'>
                        <button type="button" className="btn bg-light m-5 back-btn d-flex justify-content-evenly align-items-center">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-left mr-5" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z" />
                        </svg>
                        Back
                    </button></Link>
                    {
                        <div className='d-flex justify-content-evenly'>
                            <img src={this.props.location.anil.data.country.flags.png} alt='country flag' className='flag-img' />
                            <section className='d-flex flex-column details-box' >
                                <h2 className='mb-4'>{this.props.location.anil.data.country.name.common}</h2>
                                <div className='d-flex justify-content-between'>
                                    <section>
                                        <h6>Native Name : <span className='text'>{this.props.location.anil.data.country.name.official}</span></h6>
                                        <h6>Population : <span className='text'>{this.props.location.anil.data.country.population}</span></h6>
                                        <h6>Region : <span className='text'>{this.props.location.anil.data.country.region}</span></h6>
                                        <h6>Sub Region : <span className='text'>{this.props.location.anil.data.country.subregion}</span></h6>
                                        <h6>Capital : <span className='text'>{this.props.location.anil.data.country.capital}</span></h6>
                                    </section>
                                    <section>
                                        <h6>Top Level Domain : <span className='text'>{this.props.location.anil.data.country.tld}</span></h6>
                                        <h6>Currencies : <span className='text'>{Object.values(this.props.location.anil.data.country.currencies)[0].name}</span></h6>
                                        <h6>Languages : <span className='text'> 
                                        {Object.values(this.props.location.anil.data.country.languages).map((lang,index)=>{
                                            return <p key={index} className='ml-2'>{lang}</p>
                                        })}
                                        </span></h6>
                                    </section>
                                </div>
                                <footer className=' d-flex mt-3'>
                                    <h6> Border Countries</h6>
                                    {
                                        this.props.location.anil.data.country.borders ? this.props.location.anil.data.country.borders.map((place)=>{
                                            return <div key={place} className='ml-2 borders bg-light'>{place}</div>
                                        }) : <p style = {{marginLeft:'2vw'}}>No boundry countries</p>
                                    }
                                </footer>
                            </section>
                        </div>
                    }
                    </div>
                
                </>
                )
    }
}

                export default CountryDetails;