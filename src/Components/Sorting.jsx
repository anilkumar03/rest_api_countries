import React, { Component } from 'react';

class SortingData extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    render() {
        return (
            <select onChange={this.props.handleSort} id="regionSelect" className="filter-bar w-30 bg-light" style = {{marginRight:"8%"}}>
                <option value="Filter by Region">Filter by Region</option>
                <option value="Africa">Africa</option>
                <option value="Americas">America</option>
                <option value="Asia">Asia</option>
                <option value="Europe">Europe</option>
                <option value="Oceania">Oceania</option>
            </select>
        );
    }
}

export default SortingData;