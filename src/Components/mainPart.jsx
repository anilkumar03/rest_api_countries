import React, { Component } from 'react';
import {Link} from "react-router-dom"
import SearchBar from './Search';
import SortingData from './Sorting'

class MainPart extends Component {
    state = {
        countries: [],
        userInput: '',
        loading: false,
        theme: false
    }

    componentDidMount() {
        fetch("https://restcountries.com/v3.1/all")
            .then((res) => res.json())
            .then((jsonData) => {
                this.setState({
                    countries: jsonData,
                    loading: true
                })
            })
    }

    searchCountry = (e) => {
        this.setState({
            userInput: e.target.value
        })
    }

    sorting = (e) => {
        let selectedRegion = e.target.value;
        if (selectedRegion === 'Filter by Region') {
            fetch("https://restcountries.com/v3.1/all")
                .then((res) => res.json())
                .then((jsonData) => {
                    this.setState({
                        countries: jsonData,
                        loading: true
                    })
                })
        }
        else {
            let selectedRegion = e.target.value;
            fetch(`https://restcountries.com/v3.1/region/${selectedRegion}`)
                .then((res) => res.json())
                .then((jsonData) => {
                    this.setState({
                        countries: jsonData,
                        loading: true
                    })
                })
        }

    }

    render() {
        const { countries, userInput, loading, theme} = this.state
        if (!loading) return <div className='d-flex flex-row justify-content-center align-items-center'>
            <h4>Please wait a moment...</h4>
        </div>
        let background = theme?"bg-dark":"bg-light"
        return (
            <div className='m-1'>
                <nav className='d-flex justify-content-between m-3'>
                    <SearchBar searchCountry={this.searchCountry} theme={this.state.theme}/>
                    <SortingData handleSort={this.sorting} />
                </nav>
                <section className={`grid ${background} bg-size-cover text-align-right`}>
                    {
                        countries
                            .filter((country) => {
                                return country.name.official.toLowerCase().includes(userInput.toLowerCase())
                            })
                            .map((country) => {
                                let countryName = country.name.common
                                const { cca2, population, region, capital } = country
                                return (
                                    <article key={cca2} className='card m-3'>
                                        <img src={country.flags.png} alt='country flag' className='mb-4' />
                                        <h6>
                                            <Link to={{pathname: `/:${countryName.toLowerCase()}`, anil:{data: {country}}}}>{countryName}</Link> 
                                        </h6>
                                        <h6>Population: <span className='text'>
                                            {population}
                                        </span></h6>
                                        <h6> Region: <span className='text'>
                                            {region}
                                        </span></h6>
                                        <h6>Capital: <span className='text'>
                                            {capital}
                                        </span></h6>
                                    </article>
                                )
                            })
                    }
                </section>
            </div>

        );
    }
}

export default MainPart;