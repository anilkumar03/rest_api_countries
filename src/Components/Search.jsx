import React, { Component } from 'react';

class SearchBar extends Component {
    render() {
        return (
            <div style = {{marginLeft:"8%"}}>
                <input type="search"
                    placeholder='   Search a country...'
                    onChange={this.props.searchCountry}
                />
            </div> 
        );
    }
}

export default SearchBar;