import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import './App.css';
import Header from './Components/Header';
import MainPart from './Components/mainPart';
import CountryDetails from './Components/CountryDetails';
import NotFound from './Components/NotFound';


class App extends Component {

  render() {

    return (

      <BrowserRouter className='p-2'>
        <Switch />
        <Header />
        <Route exact path="/" component={MainPart} />
        <Route exact path="/:countryName" component={CountryDetails} />
        <Route component={NotFound} />
        <Switch />
      </BrowserRouter>

    );
  }
}

export default App;
